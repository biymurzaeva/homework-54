import OneSquare from "../OneSquare/OneSquare";
import './PlayingField.css';

const PlayingField = props => {
    const cells = props.cell.map((square) => (
        <OneSquare
            key={square.id}
            open={(e) => {props.openSquare(e.target, square.id)}}
            styleBtn={props.styleBtn(square.id)}
        />
    ));

    return (
        <div className="game-block">
            {cells}
        </div>
    );
};

export default PlayingField;