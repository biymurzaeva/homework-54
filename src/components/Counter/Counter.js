import React from 'react';

const Counter = ({count}) => {
    return (
        <p style={{textAlign: 'center'}}>
           Tries: {count}
        </p>
    );
};

export default Counter;