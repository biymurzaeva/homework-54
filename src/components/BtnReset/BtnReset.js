import React from 'react';

const BtnReset = ({reset}) => {
    return (
        <div style={{textAlign: 'center'}}>
            <button onClick={reset}>Reset</button>
        </div>
    );
};

export default BtnReset;