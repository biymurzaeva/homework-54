import './OneSquare.css';

const OneSquare = props => {
    let buttonClasses = ['GreyButton'];

    if (props.styleBtn) {
        buttonClasses.push('TransparentButton');
    }

    return (
        <div className={buttonClasses.join(' ')} onClick={props.open}/>
    );
};

export default OneSquare;