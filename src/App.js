import './App.css';
import PlayingField from "./components/PlayingField/PlayingField";
import {useState} from "react";
import Counter from "./components/Counter/Counter";
import BtnReset from "./components/BtnReset/BtnReset";

const App = () => {
    const createArrayCells = () => {
        const ringId = Math.floor(Math.random() * 36);

        const arrayCells = [];

        for (let i = 0; i < 36; i++) {
            if (ringId === i) {
                arrayCells.push({
                    open: false,
                    hasItem: true,
                    id: i,
                });
            } else {
                arrayCells.push({
                    open: false,
                    hasItem: false,
                    id: i,
                });
            }
        }

        return arrayCells;
    }

    const [game, setGame] = useState(createArrayCells());

    const [count, setCount] = useState({
        count: 0,
    });

    const isOpen = (id) => {
        return game[id].open === true
    };

    const open = (element, id) => {
        setCount({count: count.count + 1});

        setGame(game.map((c) => {
             if (c.id === id) {
                if (c.hasItem === true) {
                    element.innerText = 'O';
                }

                return {
                    ...c,
                    open: true,
                };
            }

            return c;
        }));
    };

    const resetGame = () => {
        setCount({count: 0});

        const arr = createArrayCells();

        const findRing = document.getElementsByClassName('GreyButton');

        for (let i = 0; i < findRing.length; i++) {
            if (findRing[i].innerHTML) {
                findRing[i].innerHTML = '';
            }
        }

        const gameCopy = [...game];
        gameCopy.splice(0, gameCopy.length, ...arr);
        setGame(gameCopy);
    };

    return (
        <div className="container">
            <PlayingField
                cell={game}
                openSquare={open}
                styleBtn={isOpen}
            />
            <Counter
                count={count.count}
            />
            <BtnReset
                reset={resetGame}
            />
        </div>
    );
};

export default App;
